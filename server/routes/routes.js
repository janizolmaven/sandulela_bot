var express = require("express"),
  router = express.Router();

  const user=require("../api/users/userController");
  const request=require("../api/requests/requestController");
  const state=require("../api/states/stateController");

  router.use("/user",user);
  router.use("/request",request);
  router.use("/state",state);

  module.exports = router;
