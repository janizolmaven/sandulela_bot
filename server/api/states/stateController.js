const express = require('express');
const router = express.Router();
const StateService = require('./stateService');

const stateService = new StateService();

router.route('/').post((req, res) => {
    stateService
        .addState(req.body)
        .then(
            request => res.status(200).send(request)
        )
        .catch(err => res.status(400).send({
            error: err.message
        }))
    console.log(req.body)
});

router.route('/seed').post((req, res) => {
    stateService
        .seedStates()
        .then(
            request => res.status(200).send(request)
        )
        .catch(err => res.status(400).send({
            error: err.message
        }))
    console.log(req.body)
});


module.exports = router;