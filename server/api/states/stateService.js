 const stateSchema=require('./stateSchema')

module.exports = class StateService {

    addState(request) {
        return stateSchema.State.create(request);
    }

    seedStates() {
        var data=[
            {
            "id": 1,
            "name": "welcome",
            "text": "Welcome, please choose from below",
            "options":
                [
                    {"1":{"2":"Claim free basic electricity"}},
                    {"2":{"3":"Resend token"}},
                    {"3":{"4":"Chat to Agent"}}
                ]
            },
            {
                "id": 2,
                "name": "claim",
                "text": "Please respond with meter number",
                "options":
                    [
                        {"1":{"1":"Back"}}
                    ]
            },
            {
                "id": 3,
                "name": "resend",
                "text": "Which token would you like?",
                "options":
                    [
                        {"1":{"1":"Back"}}

                    ]
            },
            {
                "id": 4,
                "name": "agent",
                "text": "To speak to an agent call (XXX)XXX-XXXX",
                "options":
                    [
                        {"1":{"1":"Back"}}
                    ]
            },
            {
                "id": 5,
                "name": "claim_response_fail",
                "text": "Your meter number verification failed. ",
                "options":
                    [
                        {"1":{"2":"Re-enter number"}},
                        {"2":{"1":"Home"}}
                    ]
            },
            {
                "id": 6,
                "name": "claim_response_waiting",
                "text": "We are waiting for server response. We will validate your meter number shortly.",
                "options":
                    [

                    ]
            },
            {
                "id": 7,
                "name": "claim_response_api",
                "text": "We are validating your meter number. We will confirm shortly",
                "options":
                    [

                    ]
            },
            {
                "id": 8,
                "name": "claim_response_pass",
                "text": "Meter for {{Name}} found at {{address}}, please confirm.",
                "options":
                    [
                        {"1":{"9":"Details correct. Claim Free Basic electricity"}},
                        {"2":{"2":"Re-enter meter number"}},
                        {"3":{"1":"Details correct"}}
                    ]
            },
            {
                "id": 9,
                "name": "token_response_api",
                "text": "We are waiting for your token. We will confirm shortly",
                "options":
                    [

                    ]
            },
            {
                "id": 10,
                "name": "token_response_waiting",
                "text": "We are waiting the server response. We will send your token shortly",
                "options":
                    [

                    ]
            },
            {
                "id": 11,
                "name": "token_response_pass",
                "text": "Please find you token: {{token}}",
                "options":
                    [

                    ]
            }]
        this.addState(data)
    }

    getStateData(state = 1, requestData){
        return new Promise((resolve, reject) => {
            stateSchema.State.findOne({id: state}, (error, data) => {
                if (error) {
                    reject(error)
                } else {
                    console.log("got state data")
                    console.log(data)
                    resolve(data)
                }
            })
        })
    }

};
