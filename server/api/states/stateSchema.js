const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const StateSchema = new mongoose.Schema({
    id:{
        type:Number
    },
    name: {
        type: String
    },
    text: {
        type: String
    },
    options: {
        type: [{}]
    },
    created: {
        type: Date,
        default: Date.now
    },
},{strict: true})
// UserSchema.path('email').index({
//     unique: true
// });
// db.users.createIndex({username:1}, {unique:true})

let States = mongoose.model('State', StateSchema);
module.exports = {
    State: States
}