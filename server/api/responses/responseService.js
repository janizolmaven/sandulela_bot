 const responseSchema=require('./responseSchema');
 const MessageService = require('../messages/messageService');

 const messageService = new MessageService();

module.exports = class ResponseService {

    generateResponse(requestData, stateData, state){
        return new Promise((resolve, reject) => {
            //get state response
            var text = stateData.text;
            //get text and save message
            messageService.saveMessage(requestData._id, text, 'out', state)
            //before calling send request I need to know if this is going to WA or HPS
            var to = "WA"
            if(stateData.name == "claim_response_api" || stateData.name == "token_response_api"){
                to = "HPS"
            }
            // First check botapi state and if need be change it.


            //prepare data object to pass
            if(to == "HPS"){
                var action;
                if (stateData.name == "token_response_api"){
                    action = "claimFbe";
                } else {
                    action = "customerLookup";
                }
                var hps_data = {
                    "meter_number": "07124753265"
                }
                var data = {
                    "client_id": requestData.client_phone,
                    "text": "",
                    "channel_id": "1234",
                    "action": action,         // This needs to be integrated
                    "data": hps_data            // This needs to be integrated
                }
            } else {
                var data = {
                    "client_id": requestData.client_phone,
                    "text": text,
                    "channel_id": "1234",
                    "action": "",         // This needs to be integrated
                    "data": ""            // This needs to be integrated
                }
            }

            //Then  call send request
        })
    }

};
