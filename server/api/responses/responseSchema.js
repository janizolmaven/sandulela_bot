const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ResponseSchema = new mongoose.Schema({
    client_id: {
        type:String,
        required:true
        //Phone number
    },
    text:{
        type:String
    },
    channel_id:{
        type:Number
    },
    action:{
        type:String
        /*
        0 - (NULL)
        1 - customerLookup (For HPS API call)
        2 - claimFbe (For HPS API call)
        3 - fetchMeters (Internal lookup)
        4 - fetchRequestRefs (Internal lookup)
        5 - reprintRequest (For HPS API call - based on request ref number)
         */
    },
    data: {
        type: {}
    },
    created: {
        type: Date,
        default: Date.now
    },
},{strict: true})
// UserSchema.path('email').index({
//     unique: true
// });
// db.users.createIndex({username:1}, {unique:true})

let Responses = mongoose.model('Response',ResponseSchema);
module.exports = {
    Response: Responses
}