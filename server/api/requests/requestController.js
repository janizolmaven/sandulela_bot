const express = require('express');
const router = express.Router();
const RequestService = require('./requestService');
const UserService = require('../users/userService');

const requestService = new RequestService();
const userService = new UserService();

router.route('/').post((req, res) => {
    requestService
        .addRequest(req.body)
        .then(
            request => res.status(200).send(request)
        )
        .catch(err => res.status(400).send({
            error: err.message
        }))
    console.log(req.body)
    var requestData = req.body
    //res.status(200).send(requestData)
    userService
        .checkUser(requestData)
        .then()
        .catch()
});


module.exports = router;