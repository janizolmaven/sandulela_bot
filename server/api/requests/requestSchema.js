const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const RequestSchema = new mongoose.Schema({
    client_phone: {
        type:String,
        required:true
    },
    text:{
        type:String
    },
    channel_id:{
        type:Number
    },
    action:{
        type:String
        /*
        0 - (NULL)
        1 - customerLookup (For HPS API call)
        2 - claimFbe (For HPS API call)
        3 - fetchMeters (Internal lookup)
        4 - fetchRequestRefs (Internal lookup)
        5 - reprintRequest (For HPS API call - based on request ref number)
        6 - changeState = channel_id:"which channel to change to", action:"changeState", client_id:"phone number", state:"default/HPS"}
         */
    },
    data: {
        type: {}
    },
    created: {
        type: Date,
        default: Date.now
    },
},{strict: true})
// UserSchema.path('email').index({
//     unique: true
// });
// db.users.createIndex({username:1}, {unique:true})

let Requests = mongoose.model('Request', RequestSchema);
module.exports = {
    Request: Requests
}