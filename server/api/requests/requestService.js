 const requestSchema=require('./requestSchema');

module.exports = class RequestService {

    addRequest(request) {
        return requestSchema.Request.create(request);
    }

};
