const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = new mongoose.Schema({

    msisdn: {
        type: String
    },
    last_state: {
        type: Number
    },
    created: {
        type: Date,
        default: Date.now
    },
    updated: {
        type: Date,
        default: Date.now
    },
},{strict: true})

let Users = mongoose.model('User', UserSchema);
module.exports = {
    User: Users
}