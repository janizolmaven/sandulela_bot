 const userSchema=require('./userSchema');
 const MessageService = require('../messages/messageService');
 const StateService = require('../states/stateService');

 const messageService = new MessageService();
 const stateService = new StateService();

module.exports = class UserService {

  checkUser(requestData){
    return new Promise((resolve, reject) => {
      //console.log(requestData);
      //console.log('checking user - get users ' + requestData.client_phone);
      //var User = mongoose.model('User', '../models/User');

      userSchema.User.findOne({msisdn: requestData.client_phone}, (error, data) => {
        if (error) {
          reject(error)
        } else {
          //console.log("heres data - "+data)
          resolve(data)
          var state
          if (data == null) {
            this.createUser(requestData)
                .then(result => {
              //console.log("resukt -user == "+result.insertedId)
              messageService.saveMessage(result.insertedId, requestData.text, 'in', 1)
            }).catch(err => reject(err))
            //console.log("Create user")
            //createUser(requestData)
            state = 0
            console.log("END if data == null")
          } else {
            //console.log("Current user");
            //save message
            messageService.saveMessage(data._id, requestData.text, 'in', data.last_state)
            state = data.last_state
            //Work out response
            console.log("END else data == null")
          }
//console.log("after got state");
          stateService.getStateData(state, requestData)
              .then(result => {
                data = result
                ///validate text and strip spaces then compare to options
                var cleantext = requestData.text.replace(/\s/g,'');
                console.log("cleaned "+cleantext)
                /* DEBUGGING
                var optionData = JSON.stringify(data.options)
                console.log("option value "+optionData)
                  console.log(typeof(data.options));
                console.log(Object.keys(data.options))
                 */
                if(data.name == "claim" && data.options[0][cleantext] == null){
                  // if you are at claim state and your response isn't a valid option it should be a meter number
                  //meter number is number 11-13 characters
                  console.log("This is a claim so we expect a meter number and need to validate it");
                  if (/^[0-9]{11,13}$/.test(cleantext)){
                    console.log("Valid meter");
                    //Send api call

                  } else {
                    console.log("Invalid meter number. Meter number must be number between 11-13 character.")
                    //Send invalid WA response, send last state message

                  }
                } else if (data.options[0][cleantext] == 0){
                  //First interaction, send welcome

                }else {
                  console.log("state data = "+data.options[0][cleantext]);
                  //check response if it is a valid option for current state
                  if(data.options[0][cleantext] != null){
                    console.log("If != null => "+data.options[0][cleantext]);
                    //save new state to user
                    //get user _id by number
                    //save update last_state for user with _id
                    console.log("state id = "+Number(Object.keys(data.options[0][cleantext]).toString()))
                    var new_state = Number(Object.keys(data.options[0][cleantext]).toString())
                      console.log("before updatestate")
                      this.updateState(requestData.client_phone, new_state)
                      console.log("after updatestate")
                    //send new state response to user WA
                    //generateResponse(requestData, data, state)
                    //Store message
                  } else {
                    console.log("Invalid option")
                    //Send invalid WA response, send last state message

                  }
                }
              }).catch(err => reject(err))
        }
      })
    })
  }

  createUser(requestData){
    return new Promise((resolve, reject) => {
        console.log("creating user");
      var newUser = { msisdn: requestData.client_phone, last_state: 1};
      console.log(newUser)
      userSchema.User.create(newUser,function(err, res) {
        if (err) {
          reject(err);
        };
        console.log(res);
        console.log('user created')
        messageService.saveMessage(res._id, requestData.text, 'in', res.state)
        resolve(res);
        //db.close();
        //console.log(res);
      })
    })
  }

  updateState(phone, state){
    console.log("Update state")
    this.getUserId(phone).then(result => {
      console.log("id = "+result)
      console.log("state = "+state);
      userSchema.User.updateOne(
          { _id: result },
          {
            $set: {
              "last_state":state,
              "updated": new Date()
            }
          },
          { upsert: false }
      )
      console.log('State updated');
    }).catch(err => reject(err))
  }

  getUserId(phone){
    return new Promise((resolve, reject) => {
      //console.log(requestData);
      console.log('checking user - get users ' + phone);
      //var User = mongoose.model('User', '../models/User');

      userSchema.User.findOne({msisdn: phone}, (error, data) => {
        if (error) {
          reject(error)
        } else {
          //console.log("Got user")
          var user_id = data._id;
          //console.log(user_id);
          //resolve(data._id)
          resolve(user_id)
        }
      })
    })
  }
};
