const express = require('express');
const router = express.Router();
const UserService = require('./userService');

const userService = new UserService();

module.exports = router;