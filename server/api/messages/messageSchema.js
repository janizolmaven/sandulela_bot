const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MessageSchema = new mongoose.Schema({
    user: {
        type: String
    },
    text: {
        type: String
    },
    type: {
        type: ["in","out"]
    },
    state: {
        type: String
    },
    created: {
        type: Date,
        default: Date.now
    },
},{strict: true})
// UserSchema.path('email').index({
//     unique: true
// });
// db.users.createIndex({username:1}, {unique:true})

let Messages = mongoose.model('Message', MessageSchema);
module.exports = {
    Message: Messages
}