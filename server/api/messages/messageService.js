 const messageSchema=require('./messageSchema');

module.exports = class MessageService {
    saveMessage(user, text, type, state){
        return new Promise((resolve, reject) => {
            var newMessage = { user: user, text: text, type: type, state: state}
            messageSchema.Message.create(newMessage,function(err, res) {
                if (err) reject(err);
                //console.log(res);
                console.log('message added')
                resolve(res);
            })
        })
    }
};
