const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
const routes = require('./routes/routes');
const config = require('config');
const { port, root } = config.get('api');
const { url } = config.get('db');
var corsOptions = {
    //origin: 'http://localhost:4200',
    origin: '*',
    optionsSuccessStatus: 200
}

// Connecting mongoDB Database
mongoose.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
        console.log("MongoDB Connected…")
    })
    .catch(err => console.log(err))


const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());
//app.use('/students', studentRoute)
app.use(`${root}`, cors(corsOptions), routes);

app.use(function (req, res, next) {
    res.status(400).send({ status: false, message: 'Request Not Found..!' });
    next();
});
app.use(function (error, req, res, next) {
    console.log(error);
    res.status(400).send({ status: false, message: 'Internal Server Error..!' });
});

app.listen(port, () => {
    console.log("HTTP Server running on port " + port);
});

app.set('trust proxy', true);
module.exports = app;