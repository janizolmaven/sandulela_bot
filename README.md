Pull code

Install Mongodb

Go into backend folder

`cd backend`

`npm install`

`mongodb/bin/mongod --dbpath data/db/`

`nodemon server.js`

Do a seed for states.

What should the code do?

1) Read request from botapi
2) Check if this is a new user
2.1) If there is no user add user
2.2) Else get user last_state
3) Based on last_state check options: options{user input:{state_id: text for user}
4) Read text from request and see if it is a valid response based on options for that state. NB - Bear in mind some states we may expect 11-13 character text
5.1) If text is valid then set new last_state for user to state_id for option
5.2) If text isn't valid response prepare invalid text
5.3) If it is a valid request that required HPS prepare data
6) Based or state or if invalid generate response to user which is a request to botapi  
7) Check whether Botapi state is on default or HPS
8) Set Botapi to the state you need it for request
9) Send request